public class Gadget extends Widget {
    // Member variables
    private String description;

    // Constructors
    public Gadget(String name, double price) {
        super(name, price); //calls the Widget argument based constructor
        //Change the name so that it includes "Gadget" at the end
        setName(name + " Gadget");
    }

    // Class methods
    protected void setDescription(String description) {
        
    }

}